#include <QCoreApplication>
#include <stdlib.h>
#include <chrono>
#include <time.h>
#include <iostream>
#include <fstream>
using namespace std;
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    ofstream* fichier;
    fichier= new ofstream("C:\\Users\\J\\Desktop\\schedTest\\in\\conf.txt",ios::app);
    int nb=1000000;
    srand (chrono::system_clock::now().time_since_epoch().count());
    vector<vector<int>> plain(4,vector<int>(4));
    vector<vector<int>> key(4,vector<int>(4));

    for(int i=0;i<nb;i++){
        string plainStr="";
        string keyStr="";
        for(int j=0;j<4;j++){
            for(int k=0;k<4;k++){
                int plainVal = rand()%256;
                plainStr+=to_string(plainVal)+ (k==3?"\n":" ");
                plain.at(j).at(k)=plainVal;

                int keyVal = rand()%256;
                keyStr+=to_string(keyVal)+ (k==3?"\n":" ");
                key.at(j).at(k)=keyVal;
            }
        }

        *fichier<<"plaintext"<<endl<<plainStr<<endl;
        *fichier<<"key"<<endl<<keyStr<<endl;


    }
    fichier->close();
    cout<<"over"<<endl;
    return a.exec();
}

void exportTasksToFile(ofstream* fichier, string str) {
    if (fichier->is_open())
    {
        *fichier<<str<<endl;
    }
    else
        cerr << "Couldn't write to outputfile '" << *fichier << "'" << endl;
}
